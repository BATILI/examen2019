package org.batili.exo1;

import org.batili.model.Commune;

public class Question4 {

	public static void main(String[] args) {

		Commune c1 = new Commune(90078,"PETITEFONTAINE",90360,"PETITEFONTAINE","");
		Commune c2 = new Commune(91471,"ORSAY",91400,"ORSAY","ORSIGNY");
		Commune c3= new Commune(91471,"ORSAY",91400,"ORSAY","ORSIGNY");
		System.out.println(c1.toString());
		System.out.println(c2.toString());
		System.out.println(c3.toString());
		if (c1.equals(c2))
			System.out.println("c1 & c2 sont egaux ");
		else System.out.println("c1 & c2 sont sont differents");
		if (c1.equals(c3))
			System.out.println("c1 & c3 sont egaux ");
		else System.out.println("c1 & c3 sont sont differents");
		if (c3.equals(c2))
			System.out.println("c3 & c2 sont egaux ");
		else System.out.println("c3 & c2 sont sont differents");
	}

}
