package org.batili.exo2;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipInputStream;

import org.batili.model.Commune;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Question5 {
	
	public static List<String> readLinesFrom(String filename) {
		Path path = Paths.get(filename);
		try (Stream<String> lines = Files.lines(path)) {
		return lines.collect(Collectors.toList());
		} catch (IOException e) {
		System.out.println(e.getMessage());
		}
		return null;
		}
	public List<Commune> readBinaryFields(String fileName){
		
		File file = new File("data-communes.csv.gz");
		List<Commune> commune = new ArrayList<>();
		
		try(FileInputStream fis = new FileInputStream(file);
			DataInputStream dis = new DataInputStream(fis);){
			
			int numberOfCommunes = dis.read();
			
			while(numberOfCommunes>0){
				
				int Code_commune_INSEE=dis.readInt();
				 String Nom_commune= dis.readUTF();
				 int Code_postal=dis.readInt();
			String Libelle_acheminement= dis.readUTF();
				 String Lieu_dit= dis.readUTF();
		
					commune.add(new Commune(Code_commune_INSEE, Nom_commune, Code_postal,Libelle_acheminement,Lieu_dit));
			numberOfCommunes--;
			}
		}catch (IOException e){
			e.printStackTrace();
		}
		System.out.println(commune);
		return commune;
	}
	public static void main(String[] args)  {
		
		System.out.println("Question 1: Java IO g�re les flux de donn�es, de type caract�re ou binaire, et les\r\n" + 
				"fichiers.File ,Writer, OuputStream, Reader , InputStream sont les classes de base pour la gestion de ces types de flux  ");
		System.out.println("Question 2: il faut toujours fermer ce flux ainsi de g�rer toutes les exception �ventuelles");
		System.out.println("Question 3: GZIPInputStream ");
		
		
	}
}
