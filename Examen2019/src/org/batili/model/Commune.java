package org.batili.model;

public class Commune {
	
	
private int Code_commune_INSEE;
private String Nom_commune;
private int Code_postal;
private String Libelle_acheminement;
private String Lieu_dit;

	public Commune() {
		// TODO Auto-generated constructor stub
	}
	public Commune(int Code_commune_INSEE,String Nom_commune,int Code_postal,String Libelle_acheminement,String Lieu_dit) {
		this.Code_commune_INSEE=Code_commune_INSEE;
		this.Nom_commune=Nom_commune;
		this.Code_postal=Code_postal;
		this.Libelle_acheminement=Libelle_acheminement;
		this.Lieu_dit=Lieu_dit;
	}
	public int getCode_commune_INSEE() {
		return Code_commune_INSEE;
	}

	public void setCode_commune_INSEE(int code_commune_INSEE) {
		Code_commune_INSEE = code_commune_INSEE;
	}

	public int getCode_postal() {
		return Code_postal;
	}

	public void setCode_postal(int code_postal) {
		Code_postal = code_postal;
	}

	public String getNom_commune() {
		return Nom_commune;
	}

	public void setNom_commune(String nom_commune) {
		Nom_commune = nom_commune;
	}

	public String getLibelle_acheminement() {
		return Libelle_acheminement;
	}

	public void setLibelle_acheminement(String libelle_acheminement) {
		Libelle_acheminement = libelle_acheminement;
	}

	public String getLieu_dit() {
		return Lieu_dit;
	}

	public void setLieu_dit(String lieu_dit) {
		Lieu_dit = lieu_dit;
	}
	@Override
	public String toString() {
		return "Commune [nom=" + Nom_commune + ", libelle=" + Libelle_acheminement + ", lieu dit=" + Lieu_dit + ", code=" + Code_commune_INSEE + ", code postal=" + Code_postal +  "]";
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Commune other = (Commune) obj;
		if (Nom_commune == null) {
			if (other.Nom_commune != null)
				return false;
		} else if (!Nom_commune.equals(other.Nom_commune))
			return false;
		if (Libelle_acheminement == null) {
			if (other.Libelle_acheminement != null)
				return false;
		} else if (!Libelle_acheminement.equals(other.Libelle_acheminement))
			return false; 
			if (Lieu_dit == null) {
				if (other.Lieu_dit != null)
					return false;
			} else if (!Lieu_dit.equals(other.Lieu_dit))
				return false;
		if (Code_postal != other.Code_postal)
			return false;
		return true;
	}

	
	public int compareTo(Commune o) {
		// TODO Auto-generated method stub
		if(o.Nom_commune.equals(o.Nom_commune)){
			return this.Lieu_dit.compareTo(o.Lieu_dit);		
		}
		else 
			return this.Nom_commune.compareTo(o.Nom_commune);
	}
	
	
	
public static void main(String[] args) {
	System.out.println("Question 1: un bean  est une m�thode particuli�re. Elle n'a pas de type de \r\n" + 
			"retour, et porte le m�me nom que la classe dans laquelle elle se trouve\n");
	System.out.println("Question 2: Le Code_commune_INSEE de la commune AJACCIO est :  2A004\n");
	System.out.println("Question 4: il faut ajouter la m�thode : toString\n");
	System.out.println("Question 5: la m�thode equals et compareTo \n");


}
}
